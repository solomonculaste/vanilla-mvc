function Model () {
  var vm = this;
  var tasks = JSON.parse(localStorage.getItem('tasks')) || [];

  vm.add = add;
  vm.get = get;
  vm.list = list;
  vm.edit = edit;
  vm.remove = remove;
  vm.toggle = toggle;
  vm.onSaved = null;

  function save () {
    tasks.sort(function (a, b) {
      var order = a.done === b.done ? 0 : a.done ? 1 : -1;
      var aTime = a.date;
      var bTime = b.date;
      var order2 = aTime < bTime ? -1 : aTime > bTime ? 1 : 0;
      return order || order2;
    });
    localStorage.setItem('tasks', JSON.stringify(tasks));
    if (vm.onSaved) vm.onSaved(tasks);
  };

  function add (label) {
    var task = {
      id: tasks.length ? Math.max.apply(Math, tasks.map(function(o) { return o.id; })) + 1 : 1,
      label: label,
      date: new Date().getTime(),
      done: false
    }

    tasks.push(task);
    save();
  }

  function get (id) {
    return tasks.filter(function (task) {
      return task.id === id;
    });
  }

  function list() {
    return tasks;
  }
  
  function edit (id, label) {
    tasks = tasks.map(function (task) {
      if (task.id === id) {
        task.label = label;
      }

      return task;
    });
    save();
  }

  function remove (id) {
    tasks = tasks.filter(function (task) {
      return task.id !== id;
    });
    save();
  }

  function toggle (id) {
    tasks = tasks.map(function (task) {
      if (task.id === id) {
        task.done = !task.done;
      }

      return task;
    });
    save();
  }
}
