(function () {
  /**
   * Script loader
   * @param {string} src script source
   * @param {function} callback
   * @returns void
   */
  function loadScript(src, callback) {
    var body = document.body;
    var appScript = document.querySelector('#app-script');
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = src;
    script.onreadystatechange = callback;
    script.onload = callback;
    body.insertBefore(script, appScript);
  }
  
  /**
   * App initialization
   */
  function init () {
    var model = new Model();
    var view = new View(undefined);
    new Controller(model, view);
  }
  
  /**
   * Load scripts
   */
  (function () {
    var count = 0;
    var sources = [
      'js/model.js',
      'js/view.js',
      'js/controller.js'
    ];

    sources.forEach(function (src) {
      loadScript(src, function () {
        count++;
        if (count === sources.length) {
          init();
        }
      });
    });
  })();
})();