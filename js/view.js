function View (selector, model) {
  var vm = this;

  // Public functions
  vm.render = render;
  vm.on = on;

  // Elements

  /**
   * Header
   */

  var date = new Date();
  var months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  var days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];
  vm.header = createElement('div', 'header');
  vm.title = createElement('h2');
  vm.title.textContent = 'You have nothing to do.';
  vm.subTitle = createElement('p', 'sub-heading');
  vm.subTitle.textContent = days[date.getDay()] + ', ' + months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();

  // Main form
  vm.form = createElement('form');
  vm.form.autocomplete = 'off';
  vm.input = createElement('input');
  vm.input.type = 'text';
  vm.input.name = 'task';

  // Submit button
  vm.submitButton = createElement('input', 'button');
  vm.submitButton.type = 'submit';
  vm.submitButton.value = 'add';

  vm.form.append(vm.title, vm.subTitle, vm.input, vm.submitButton);
  vm.header.append(vm.form);

  /**
   * Content
   */
  vm.content = createElement('div', 'content');

  // Task list
  vm.taskList = createElement('ul', 'tasks');
  vm.content.append(vm.taskList);
  // App container
  vm.selector = selector || '#app';
  vm.root = getElement(vm.selector);

  if (!vm.root) {
    throw new Error('Root undefined.');
  }

  vm.root.append(vm.header, vm.content);

  function createElement (tag, className) {
    var element = document.createElement(tag);
    if (className) {
      element.classList.add(className);
    }
    return element;
  }

  function getElement (selector) {
    return document.querySelector(selector);
  }

  function resetInput () {
    vm.input.value = '';
  }

  /**
   * 
   * @param {*} date 
   * Extract to own service.
   */
  function parseDate (date) {
    var parsed = new Date(date);
    return (
      parsed.getMonth() +
      '/' +
      parsed.getDate() +
      '/' +
      parsed.getFullYear() +
      ' @' +
      parsed.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    );
  }

  function render (tasks) {
    while (vm.taskList.firstChild) {
      vm.taskList.removeChild(vm.taskList.firstChild);
    }

    // Update title
    var done = tasks.filter(function (task) { return !task.done }).length;
    var message = 'You have ' + done + ' task' + (done > 1 ? 's.' : '.');
    vm.title.textContent = done ? message : 'You have nothing to do.';

    if (tasks.length) {
      tasks.forEach(function (task) {
        var li = createElement('li', 'task');

        // Checkbox
        var toggleAction = createElement('div', 'action');
        var checkbox = createElement('input');
        var label = createElement('label', 'checkbox');
        var marker = createElement('span', 'marker');
        checkbox.type = 'checkbox';
        checkbox.checked = task.done;
        checkbox.value = task.id;
        label.append(checkbox, marker);
        toggleAction.append(label);
        
        // span.contentEditable = true;
        // span.classList.add('editable');
        // span.textContent = task.label;
        // if (task.done) span.classList.add('strike-through');

        // Details
        var details = createElement('div', 'details');
        var taskName = createElement('h4');
        taskName.textContent = task.label;
        var meta = createElement('p');
        meta.textContent = parseDate(task.date);
        details.append(taskName, meta);
        
        var removeAction = createElement('div', 'action');
        var remove = createElement('a', 'delete-task');
        remove.textContent = 'delete';
        remove.setAttribute('task-id', task.id);
        removeAction.append(remove);
        
        if (task.done) li.classList.add('done');
        li.append(toggleAction, details, removeAction);
        vm.taskList.append(li);
      });
    }
    
    // else {
    //   var p = createElement('p');
    //   p.textContent = 'You have no tasks. Add one. Dont be lazy.';
    //   vm.taskList.append(p);
    // }
  }

  function on (event, handler) {
    switch (event) {
      case 'addTask':
        vm.form.addEventListener('submit', function (event) {
          event.preventDefault();
          var task = vm.input.value;
          if (task) {
            handler(task);
            resetInput();
          }
        });
        break;
      
      case 'deleteTask':
        vm.taskList.addEventListener('click', function (event) {
          if (event.target.className.includes('delete-task')) {
            var id = parseInt(event.target.attributes['task-id'].value);
            handler(id);
          }
        });
        break;
      
      case 'toggleTask':
        vm.taskList.addEventListener('change', function (event) {
          if (event.target.type === 'checkbox') {
            var id = parseInt(event.target.value);
            handler(id);
          }
        });
        break;
      
      case 'taskEdited':
        vm.taskList.addEventListener('focusout', function (event) {
          if (event.target.tagName.toLowerCase() === 'span') {
            var id = parseInt(event.target.parentElement.id);
            var task = event.target.innerText;
            handler(id, task);
          }
        });
        break;
    }
  }
}