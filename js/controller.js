function Controller (model, view) {
  var vm = this;
  vm.model = model;
  vm.view = view;

  function renderTasks (tasks) {
    vm.view.render(tasks);
  }

  function handleEvents () {
    vm.view.on('addTask', vm.model.add);
    vm.view.on('deleteTask', vm.model.remove);
    vm.view.on('taskEdited', vm.model.edit);
    vm.view.on('toggleTask', vm.model.toggle);
  }

  vm.model.onSaved = renderTasks;
  renderTasks(vm.model.list());
  handleEvents();
}